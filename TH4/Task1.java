package com.epam.test.automation.java.practice4;

import practice4.SortOrder;

public class Task1 {

    /**
     * <summary>
     * Implement code according to description of task.
     * </summary>
     * if set invalid arguments in method, then method must throws
     * IllegalArgumentException
     *
     * @return
     */
/*    public static boolean goASC(int[] array) {


    }


    public static boolean goDESC(int[] array) {

        int i;
        for (i = 0; i < array.length; i++){
            if (array[i] < array[i + 1]) {
                return true;
            } else {
                return false;
            }
        }

    }*/
    public static boolean isSorted(int[] array, SortOrder order) {

        boolean isSorted = false;
        int i;
        if (array == null || array.length <= 1) {
            throw new IllegalArgumentException();
        } else if (order == SortOrder.ASC) {

            for (i = 0; i < array.length-1; i++) {
                if (array[i] < array[i + 1]) {
                    isSorted = true;
                } else {
                    isSorted = false;
                    break;
                }
            }
        } else if (order == SortOrder.DESC) {
            for (i = 0; i < array.length-1; i++) {
                if (array[i] > array[i + 1]) {
                    isSorted = true;
                } else {
                    isSorted = false;
                    break;
                }
            }
        }
        return isSorted;
    }

}

