package practice4;

public class Task3 {

    /**
     * <summary>
     * Implement code according to description of task.
     * </summary>
     * if set invalid arguments in method, then method must throws
     * IllegalArgumentException
     */
    public static int multiArithmeticElements(int a1, int t, int n) {

        if (n <= 1) {
            throw new IllegalArgumentException();
        }
        int[] a = new int[n];
        a[0] = a1;
        int multi = a[0];
        for (int i = 1; i < n ; i++) {
           a[i] = a[i-1]+t;
           multi = multi * a[i];
        }
        return multi;
    }

}
