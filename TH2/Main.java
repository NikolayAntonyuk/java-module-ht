package com.epam.test.automation.java.practice2;

import java.util.Scanner;

public class Main {

    /**
     * <summary>
     * Implement code according to description of task 1.
     * </summary>
     */
    public static String exceptionMess = "Value is not allowed ";
    public static int task1(int value) {
        if (value <= 0) {
            throw new IllegalArgumentException(exceptionMess + value);
        } else {
            int oddSum = 0;
            while (value > 0) {
                int x = value % 10;
                if (x % 2 != 0) {
                    oddSum += x;
                }
                value = value / 10;
            }
            return oddSum;
        }
    }
       /* String str = Integer.toString(value);
        int sum = 0;
        for (int i = 0; i < str.length(); i++) {
            char letterValue = str.charAt(i);
            int D = Integer.parseInt(String.valueOf(letterValue));
            if (D % 2 != 0) {
                sum += D;
            }
        }
        return sum;*/


    /**
     * <summary>
     * Implement code according to description of task 2.
     * </summary>
     */
    public static int task2(int value) {
        if (value <= 0) {
            throw new IllegalArgumentException(exceptionMess + value);
        }
        else {
            int result = 0;
            for (; value > 0; value = value >> 1)
                result += ((value & 1) == 1 ? 1 : 0);
            return result;
        }
    }

    /**
     * <summary>
     * Implement code according to description of task 3.
     * </summary>
     */
    public static int task3(int value) {
        if (value <= 1) {
            throw new IllegalArgumentException(exceptionMess + value);
        } else {
            int fibo[] = new int[value + 1];
            fibo[0] = 0;
            fibo[1] = 1;
            int sum = fibo[0] + fibo[1];
            for (int i = 2; i < value; i++) {
                fibo[i] = fibo[i - 1] + fibo[i - 2];
                sum += fibo[i];
            }
            return sum;
        }
    }
}

