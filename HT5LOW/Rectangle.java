package practice5;

public class Rectangle {

    private double a;
    private double b;

    public Rectangle(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public Rectangle(double a) {

        this.a = a;
    }

    public Rectangle() {
        this.a = 4;
        this.b = 3;
    }

    public double getSideA() {
        return a;
    }

    public double getSideВ() {
        return b;
    }

    public static double area(double a, double b) {
        double y = a * b;
        return y;
    }

    public static double perimeter(double a, double b) {
        double perimeter = 2 * (a + b);
        return perimeter;
    }

    public static boolean isSquare(double a, double b) {
        if (a == b)
            return true;
        else
            return false;
    }

    public void replaceSides(double a, double b) {
        double replaceSides = a;
        this.a = b;
        this.b = replaceSides;
    }

}
