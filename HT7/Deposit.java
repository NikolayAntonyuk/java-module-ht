package practice7;

import java.math.BigDecimal;

public abstract class Deposit<E> implements Comparable<Deposit> {
    private final BigDecimal amount;
    private final int period;

     Deposit(final BigDecimal amount,final int period) {
        this.amount = amount;
        this.period = period;
    }
    public abstract BigDecimal income();



    public BigDecimal getAmount() {
        return amount;
    }



    public int getPeriod() {

        return period;
    }




}

