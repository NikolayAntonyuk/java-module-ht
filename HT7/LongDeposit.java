package practice7;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class LongDeposit extends Deposit implements Prolongable {
    public LongDeposit(BigDecimal amount, int period) {
        super(amount, period);
    }

    @Override
    public BigDecimal income() {
        if (getPeriod() <= 0) {
            throw new IllegalArgumentException();
        } else {
            double p = 0.15;
            BigDecimal percent;
            BigDecimal arrAmount[] = new BigDecimal[getPeriod()];
            for (int i = 0; i < getPeriod(); i++) {
                if (i < 6) {
                    arrAmount[i] = getAmount();
                } else {
                    percent = arrAmount[i - 1].multiply(new BigDecimal(p));
                    arrAmount[i] = percent.add(arrAmount[i - 1]);
                }
            }
            return arrAmount[getPeriod() - 1].subtract(getAmount()).setScale(2, RoundingMode.HALF_EVEN);
        }
    }

  @Override
   public boolean canToProlong() {
        int max3years = 12*3;
        if (getPeriod() <= max3years) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
