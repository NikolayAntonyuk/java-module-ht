package practice7;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class SpecialDeposit extends Deposit implements Prolongable {
    public SpecialDeposit(BigDecimal amount, int period) {
        super(amount, period);
    }

    @Override
    public BigDecimal income() {
        double p = 0.01;
        BigDecimal percent;
        if (getPeriod() <= 0) {
            throw new IllegalArgumentException();
        } else {
            BigDecimal arrAmount[] = new BigDecimal[getPeriod()];
            arrAmount[0] = getAmount().add(getAmount().multiply(new BigDecimal(p)));
            for (int i = 1; i < getPeriod(); i++) {
                p=p+0.01;
                percent = arrAmount[i - 1].multiply(new BigDecimal(p));
                arrAmount[i] = percent.add(arrAmount[i-1]);
            }
            return arrAmount[getPeriod()-1].subtract(getAmount()).setScale(2, RoundingMode.HALF_EVEN);
        }
    }


    @Override
    public String toString() {
        return "SpecialDeposit{}";
    }

    @Override
    public boolean canToProlong() {
        double sumMin = 1000.00;
        if (getAmount().compareTo(getAmount()) >= sumMin) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
