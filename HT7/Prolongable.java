package practice7;

public interface Prolongable {
    boolean canToProlong();
}
