package practice7;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BaseDeposit extends Deposit {
    public BaseDeposit(BigDecimal amount, int period) {

        super(amount, period);
    }


    @Override
    public BigDecimal income() {
    final var prcent = BigDecimal.valueOf(1.05);
        BigDecimal x = getAmount();
        for (int i = 0; i < getPeriod() ; i++) {
            x = x.multiply(prcent);
        }
        return x.subtract(getAmount()).setScale(2, RoundingMode.HALF_EVEN);
    }






    @Override
    public int compareTo(Object o) {
        return 0;
    }
}