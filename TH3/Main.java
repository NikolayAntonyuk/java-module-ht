package practice3;

import java.util.HashMap;

public class Main {

    /**
     * <summary>
     * Implement code according to description of task 1.
     * </summary>
     */
    public static int[] task1(int[] array) {

        for (int i = 0; i < array.length / 2; i++) { // Here!
            if (array[i] % 2 == 0 && array[array.length - 1 - i] % 2 == 0) {
                int temp = array[i];
                array[i] = array[array.length - 1 - i];
                array[array.length - 1 - i] = temp;
            }
        }
            return array;
    }


    /**
     * <summary>
     * Implement code according to description of task 2.
     * </summary>
     */
    public static int task2(int[] array) {
        int maxDist = 0;
        for(int i=0; i<array.length; i++)
        {
            int firstOcc = -1;
            int lastOcc = -1;

            for(int j=0; j<array.length; j++)
            {
                if(array[i] == array[j])
                {
                    if(firstOcc == -1)
                        firstOcc = lastOcc = j;
                    else
                        lastOcc = j;
                }
            }
            if(lastOcc - firstOcc > maxDist)
                maxDist = lastOcc - firstOcc;
        }
        return maxDist;
    }

    /**
     * <summary>
     * Implement code according to description of task 3.
     * </summary>
     */
    public static int[][] task3(int[][] matrix) {
        for (int i = 0; i < matrix.length ; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (i > j) matrix[i][j] = 0;
                else if (i<j) matrix[i][j] = 1;
             }
        }
        return matrix;
    }

}

