package practice6;

public class Employee {

    private String name;
    private int salary;
    private int bonus;

    public Employee(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }

    public double toPay(int salary, int bonus){
        return  salary + bonus;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(int bonus) {
        this.bonus = bonus;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", salary=" + salary +
                ", bonus=" + bonus +
                '}';
    }
}
