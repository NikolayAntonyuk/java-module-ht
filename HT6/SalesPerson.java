package practice6;

public class SalesPerson extends Employee{
    private int percent;

    public SalesPerson(String name, int salary, int percent) {
        super(name, salary);
        this.percent = percent;
    }

    @Override
    public void setBonus(int bonus) {
        super.setBonus(bonus);
        if (percent >= 100 && percent  < 200)
            setBonus(bonus*2);
            if (percent >= 200)
                setBonus(bonus*3);
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    @Override
    public String toString() {
        return "SalesPerson{" +
                "percent=" + percent +
                '}';
    }
}
