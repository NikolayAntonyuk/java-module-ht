package practice6;

import java.util.Arrays;

public class Company extends Employee {
    private double[] employees;
    private int count;

    public Company(String name, int salary, double[] employees, int count) {
        super(name, salary);
        this.employees = employees;
        this.count = 0;
    }




    public double TotalToPay() {
        double sum = getSalary();
        for (double i = 0; i < employees.length; i++) {
            sum = sum + employees[(int) i];
        }
        return sum;
    }

    public String NameMaxSalary(){
        String nameMaxSalary = getName();
        for (int i = 0; i < employees.length; i++) {
            if (getSalary() < employees[i]){
                nameMaxSalary = getName();
            }
        }
        return nameMaxSalary;
    }


    public double[] getEmployees() {
        return employees;
    }

    public void setEmployees(double[] employees) {
        this.employees = employees;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Company{" +
                "employees=" + Arrays.toString(employees) +
                ", count=" + count +
                '}';
    }
}

