package practice6;

public class Manager extends Employee {
       private int quantity;

    public Manager(String name, int salary) {
        super(name, salary);
        int clientAmount;
    }
    @Override
    public void setBonus(int bonus) {
        super.setBonus(bonus);
        if (quantity >= 100 && quantity  < 150)
            setBonus(bonus+500);
        if (quantity >= 150)
            setBonus(bonus+1000);
    }


    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Manager{" +
                "quantity=" + quantity +
                '}';
    }
}
