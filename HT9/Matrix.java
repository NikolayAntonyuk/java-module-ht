package practice12;

import java.io.IOException;

public class Matrix {
    private int column;
    private int row;
    private double matrix[][];

    public Matrix(int column, int row) {
        this.column = column;
        this.row = row;
        matrix = new double[row][column];
    }

    public Matrix(double matrix[][]) {
        this.matrix = matrix;
        this.row = matrix.length;
        this.column = matrix[0].length;
    }

    /**
     *
     * @param matrix1
     * @param matrix2
     * @return
     * @throws IOException
     */
    public double[][] addition(double[][] matrix1, double[][] matrix2) throws IOException {
        int rows = matrix1.length;
        int columns = matrix1[0].length;
        double[][] result = new double[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                result[i][j] = (int) (matrix1[i][j] + matrix2[i][j]);
            }
        } return result;
    }
    /**
     *
     * @param matrix1
     * @param matrix2
     * @return
     * @throws IOException
     */
    public static double[][] subtract(double[][] matrix1, double[][] matrix2) throws IOException{
        int rows = matrix1.length;
        int columns = matrix1[0].length;
        double[][] result = new double[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                result[i][j] = matrix1[i][j] - matrix2[i][j];
            }
        } return result;
    }
    /**
     *
     * @param matrix1
     * @param matrix2
     * @return
     * @throws IOException
     */
    public static Double[][] multiplication(Double[][] matrix1, Double[][] matrix2) throws IOException {

        int aRows = matrix1.length;
        int aColumns = matrix1[0].length;
        int bRows = matrix2.length;
        int bColumns = matrix2[0].length;

        if (aColumns != bRows) {
            throw new IllegalArgumentException("A:Rows: " + aColumns + " did not match B:Columns " + bRows + ".");
        }

        Double[][] C = new Double[aRows][bColumns];
        for (int i = 0; i < aRows; i++) {
            for (int j = 0; j < bColumns; j++) {
                C[i][j] = 0.00000;
            }
        }
        for (int i = 0; i < aRows; i++) {
            for (int j = 0; j < bColumns; j++) {
                for (int k = 0; k < aColumns; k++) {
                    C[i][j] += matrix1[i][k] * matrix2[k][j];
                }
            }
        }

        return C;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public double[][] getMatrix() {
        return matrix;
    }

    public void setMatrix(double[][] matrix) {
        this.matrix = matrix;
    }


}
