package practiceJava8;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Task2 {

    public static void main(String[] args) {
        List<String> getStringList = Arrays.asList("Hello", "world", "!", "Good", "morning", "!");

        getStringList.stream()
                .filter(Objects::nonNull)
                .filter(p->p.length()<12)
                .sorted()
                .forEach(s->System.out.println(s));
    }

}
