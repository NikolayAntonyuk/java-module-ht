package practiceJava8;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Task1 {

    public static void main(String[] args) {
        List<String> getStringList = Arrays.asList("Hello", "qwerty", "asda", "asdfa", " as "," a ", null);

        getStringList.stream()
                .filter(Objects::nonNull)
                .filter(stringList -> !getStringList.isEmpty() && getStringList.contains("a"))
                .forEach(System.out::println);
    }

}
